#from pynput.keyboard import Key, Listener,Controller
from pynput import keyboard
from enum import Enum
import sys,os

class TECLAS(Enum):
    ENTER = 1
    SETA_DIREITA = 2
    SETA_ESQUEDA = 3
    TECLA_ESC = 4

clear = lambda: os.system('clear')
clear()

def getKeyPressed():
    with keyboard.Events() as events:
        for event in events:
            if event.key == keyboard.Key.enter:
               return TECLAS.ENTER 
            if(event.key == keyboard.Key.left):
                return TECLAS.SETA_ESQUEDA    
            elif(event.key == keyboard.Key.right):
                return TECLAS.SETA_DIREITA
            else:
                return None

def binaryOptions(value,options):
    if(value):
        mk1 = "*"
        mk2 = " "
    else:
        mk1 = " "
        mk2 = "*"

    print(" [%s] %s [%s]  %s " % (mk1,opcoes[0], mk2,opcoes[1]))

def makeDecision(text,opcoes):
    enterTyped = False
    valueSelected = True
    while(not enterTyped):
        clear()
        print(text)
        binaryOptions(valueSelected,opcoes)
        
        sys.stdin.flush()

        key = getKeyPressed()
        if(key == TECLAS.ENTER):
            print("entrou enter")
            enterTyped=True 
        if(key == TECLAS.SETA_DIREITA):
            print("entrou false")
            valueSelected = False 
        if(key == TECLAS.SETA_ESQUEDA):
            print("entrou true")
            valueSelected = True

        sys.stdout.flush()
        
opcoes = ["qualquer coisa 1","qualquer coisa 2"]
makeDecision("texto \n pra aparecer",opcoes)

opcoes = ["qualquer coisa 3","qualquer coisa 4"]
makeDecision("texto \n pra aparecer",opcoes)
#def jogo():
#    """Ideia do jogo de adila
#primeiro a gente da uma descricao de bem vindo
#depois a gente comeca as perguntas"""
#    print("o jogo começa aqui")
#
#def main():
#    print("""
#
#    Olá viajante!
#
#    Bem vindo ao jogo 
#    você terá uma chance de acertar 
#    e uma de errar!
#    boa sorte! se errar já sabe
#    ︻▅▅▆▆▇◤ ^(x_x)^
#          """)
#    if keyboard.is_pressed(''):
#    #input("escolha uma opcao")
#
#if __name__ == "__main__":
#   main()
